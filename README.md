# First Rest API

Une API REST créée avec le framework Spring Boot

## How to use
1. Cloner le projet
2. Modifier l'url de connexion dans le [application.properties](src/main/resources/application.properties) si besoin
3. Importer la [base de données](src/main/resources/database.sql) si besoin
4. Lancer le projet

## Exercices
### Afficher une liste d'opération en JSON ([contrôleur](src/main/java/co/simplon/promo18/firstrest/controller/OperationController.java) / [repository](src/main/java/co/simplon/promo18/firstrest/repository/OperationRepository.java))
1. Créer une base de données p18_firstrest et dans celle ci, créer une table operation qui aura un id en integer, un label en varchar, une date en Date et un amount en double
2. Dans l'application Spring, créer une entité Operation dans un package entity (qui sera dans le package co.simplon.promo18.firstrest) et lui mettre les différentes propriétés avec juste la date en LocalDate, ses getter/setters et ses constructeurs
3. Dans le OperationRepository, créer ou modifier la méthode findAll pour faire qu'elle renvoie une List<Operation> et dedans s'inspirer des repo qu'on a déjà fait pour lui faire faire un findAll
4. Créer un nouveau contrôleur OperationController en vous inspirant du ExampleController, et dedans faire une méthode all() qui va utiliser le repository pour récupérer la liste des opération et la renvoyer (rajouter le OperationRepository en propriété Autowired dans le contrôleur, commme on a fait pour la datasource) 

### Faire persister une Operation ([contrôleur](src/main/java/co/simplon/promo18/firstrest/controller/OperationController.java) / [repository](src/main/java/co/simplon/promo18/firstrest/repository/OperationRepository.java))
1. Dans le repository, créer une méthode save(Operation operation) qui va faire persister une opération en bdd (""inspirez"" vous de ce qu'on a déjà fait avec le projet jdbc)
2. Dans le OperationController, créer une nouvelle méthode add (sur l'url /api/operation toujours) en vous inspirant de la méthode postExample du ExampleController qui va donc attendre en argument une Operation et qui va la faire persister à l'aide la méthode save() du repo
3. Utiliser ThunderClient pour vérifier si le post marche bien en lui faisant faire une requête de type POST vers http://localhost:8080/api/operation et en lui mettant une operation au format json dans le body 

Bonus : Faire que si ya une date, il la fasse persister telle quelle, mais que si yen a pas il met la date de maintenant
Faire qu'on ait pas besoin de répéter l'url entre le GetMapping et le PostMapping, vu que c'est la même 

### Récupérer une opération par son id ([contrôleur](src/main/java/co/simplon/promo18/firstrest/controller/OperationController.java) / [repository](src/main/java/co/simplon/promo18/firstrest/repository/OperationRepository.java))
1. Dans le OperationController, rajouter un nouveau GetMapping qu'on va appeler one et faire qu'il renvoie une Operation
2. S'inspirer du ExampleController pour faire que cette requête soit paramétrée et attende un id dans l'url
3. Utiliser cet id pour faire un findById et renvoyer l'operation récupérée
4. Rajouter une vérification qui fait que si le findById renvoie null, alors on fait en sorte de renvoyer un 404 (aide: il faudra pour ça faire en sorte de throw une ResponseStatusException )

### Les tests ([fichier](src/test/java/co/simplon/promo18/firstrest/repository/OperationRepositoryTest.java))
En vous inspirant du [testFindAll](src/test/java/co/simplon/promo18/firstrest/repository/OperationRepositoryTest.java), faire un testFindById ainsi qu'un testSave

### Supprimer une opération par son id ([contrôleur](src/main/java/co/simplon/promo18/firstrest/controller/OperationController.java) / [repository](src/main/java/co/simplon/promo18/firstrest/repository/OperationRepository.java))
1. Dans le OperationController, rajouter un nouveau DeleteMapping
2. S'inspirer du one pour faire que cette requête soit paramétrée et attende un id dans l'url
3. Utiliser cet id pour faire un delete et supprimer l'opération récupérée (il faut au préalable avoir créé la méthode delete dans le repo)
4. Rajouter une vérification qui fait que si le deleteById renvoie false, alors on fait en sorte de renvoyer un 404 (aide:  il faudra pour ça faire en sorte de throw une ResponseStatusException)

### Modifier une opération par son id ([contrôleur](src/main/java/co/simplon/promo18/firstrest/controller/OperationController.java) / [repository](src/main/java/co/simplon/promo18/firstrest/repository/OperationRepository.java))
1. Dans le OperationController, rajouter un nouveau PutMapping
2. S'inspirer du delete pour faire que cette requête soit paramétrée et attendre un id dans l'url
3. Utiliser cet id pour faire un update et mettre à jour l'opération récupérée (il faut au préalable avoir créé la méthode update dans le repo)
4. Rajouter une vérification qui fait que si le update renvoie false, alors on fait en sorte de renvoyer un 404 (aide:  il faudra pour ça faire en sorte de throw une ResponseStatusException)
5. Rajouter une vérification qui fait que si l'id dans l'URL est différent de l'id de l'opération reçue dans la requête, on renvoie une rreur 400 BAD REQUEST

### Modifier partiellement une opération par son id
1. Dans le OperationController, rajouter un nouveau PatchMapping
2. S'inspirer du put pour faire que cette requête soit paramétrée et attendre un id dans l'url
3. Utiliser cet id pour faire un update et mettre à jour l'opération récupérée; attention, dans le patch, on ne reçoit que les champs à mettre à jour
4. Rajouter une vérification qui fait que si le update renvoie false, alors on fait en sorte de renvoyer un 404 (aide:  il faudra pour ça faire en sorte de throw une ResponseStatusException)