package co.simplon.promo18.firstrest.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import co.simplon.promo18.firstrest.entity.Person;

/**
 * L'annotation RestController indique à Spring que la classe actuelle sera un 
 * contrôleur et plus spécifiquement un contrôleur de type REST
 */
@RestController
public class ExampleController {

    /**
     * Dans les contrôleurs, on va venir indiquer les différentes ressources disponibles
     * en indiquant leur url et leur méthode HTTP selon ce que la route permet
     * de faire
     */
    @GetMapping("/api/example")
    public String firstRoute() {
        return "quelque chose";
    }

    /**
     * Le PostMapping sert en général à faire persister une nouvelle entité sur la 
     * base de données.
     * On récupère le contenu de la requête avec le RequestBody qui va automatiquement convertir
     * le JSON en une instance de la classe qu'on lui indique en paramètre
     * @param person La personne à faire persister
     * @return La personne complète une fois persistée (avec son id et autre)
     */
    @PostMapping("/api/person")
    public Person postExample(@RequestBody Person person) {

        System.out.println(person.getName());
        return person;

    }

    /**
     * On peut faire en sorte de rendre nos url paramètrables en indiquant entre
     * accolades une partie de l'url qui attendra un argument.
     * Ici pour accéder à l'url, il faudra aller sur http://localhost:8080/api/withparam/test
     * et le 'test' sera récupérer et mis dans la variable name en paramètre grâve au
     * PathVariable
     * @param name Le nom récupéré de l'url
     * @return une chaîne de caractère qui dit coucou au nom donné
     */
    @GetMapping("/api/withparam/{name}")
    public String paramExample(@PathVariable String name) {
        return "coucou "+name;
    }

}
