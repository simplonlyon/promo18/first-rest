package co.simplon.promo18.firstrest.controller;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PastOrPresent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.promo18.firstrest.entity.Operation;
import co.simplon.promo18.firstrest.repository.OperationRepository;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/operation") //Indique un préfixe qui s'appliquera à tous les Mapping du contrôleur actuel
@Validated
public class OperationController {
    @Autowired
    private OperationRepository repo;

    @GetMapping
    public List<Operation> all(@RequestParam Optional<String> label) {
        if(!label.isPresent()) {
            return repo.findAll();
        } else {
            return repo.searchByLabel(label.get());
        }
    }

    private Operation showOne(int id) {
        Operation operation = repo.findById(id);
        if(operation == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return operation;
    }

    @GetMapping("/{id}") // ex: http://localhost:8080/api/operation/1
    public Operation one(@PathVariable @Min(1) int id) {
        return this.showOne(id);
    }


    /**
     * Manière alternative de faire les routes avec verbe et paramètres, 
     * qui ne se base pas sur les standards RESTful mais est néanmoins utilisé 
     * en entreprise. Pour celle ci comme pour le /add, pas la peine de faire
     * les deux, juste on choisit son style entre REST ou RPC
     */
    @GetMapping("/show")
    public Operation show(@RequestParam @Min(1) int id) {
        return this.showOne(id);
    }

    private Operation createPost(Operation operation) {
        if(operation.getDate() == null){
            operation.setDate(LocalDate.now());
        }

        repo.save(operation);
        
        return operation;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED) //Permet de renvoyer un code plus précis dans le cas d'un ajout réussi
    public Operation add(@Valid @RequestBody Operation operation) {
        return this.createPost(operation);
    }

    /**
     * Manière alternative de faire les routes d'ajout, qui ne se base pas sur les 
     * standards RESTful mais est néanmoins utilisé en entreprise
     */
    @PostMapping("/add")
    @ResponseStatus(HttpStatus.CREATED)
    public Operation addWithParams(
        @RequestParam @NotBlank String label,
        @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @PastOrPresent LocalDate date,
        @RequestParam Double amount
    ) {
        Operation o = new Operation(label, date, amount);
        return this.createPost(o);
    }
    
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT) //Permet de renvoyer un code plus précis dans le cas d'un ajout réussi
    public void delete(@PathVariable @Min(1) int id) {
        if(!repo.delete(id)){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{id}")
    public Operation update(@Valid @RequestBody Operation operation, @PathVariable @Min(1) int id) {
        if (id != operation.getId()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        if(!repo.update(operation)){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return repo.findById(operation.getId());
    }

    @PatchMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Operation patch(@RequestBody Operation operation, @PathVariable @Min(1) int id) {
      Operation baseOp = repo.findById(id);
      if (baseOp == null) throw new ResponseStatusException(HttpStatus.NOT_FOUND);
      if (operation.getLabel() != null) baseOp.setLabel(operation.getLabel());
      if (operation.getDate() != null) baseOp.setDate(operation.getDate());
      if (operation.getAmount() != 0.0) baseOp.setAmount(operation.getAmount());
      if (!repo.update(baseOp)) throw new ResponseStatusException(HttpStatus.NOT_FOUND);
      return baseOp;
    }

}
