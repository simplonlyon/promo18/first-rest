package co.simplon.promo18.firstrest.entity;

import java.time.LocalDate;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;


public class Operation {
    private Integer id;
    @NotBlank
    private String label;
    @PastOrPresent
    private LocalDate date;
    @NotNull
    private Double amount;
    
    public Operation(String label, LocalDate date, Double amount) {
        this.label = label;
        this.date = date;
        this.amount = amount;
    }
    public Operation() {
    }
    public Operation(Integer id, String label, LocalDate date, Double amount) {
        this.id = id;
        this.label = label;
        this.date = date;
        this.amount = amount;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }
    public LocalDate getDate() {
        return date;
    }
    public void setDate(LocalDate date) {
        this.date = date;
    }
    public Double getAmount() {
        return amount;
    }
    public void setAmount(Double amount) {
        this.amount = amount;
    }
}
