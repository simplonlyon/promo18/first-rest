package co.simplon.promo18.firstrest.repository;


import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

import co.simplon.promo18.firstrest.entity.Operation;

/**
 * Pour le repository, on fait un test dit "d'intégration" qui va réellement se connecter
 * à une base de données
 * Cela nécessite donc de maintenir une base de données dans un état propre et conforme aux
 * tests (chose qu'on fait ici en supprimant et recréant la bdd en se basant sur le .sql
 * entre chaque test)
 */
@SpringBootTest
@Sql({ "/database.sql" }) //Charge le fichier SQL indiqué, permet de remettre la base de données "à zéro" entre chaque test
public class OperationRepositoryTest {

    @Autowired
    OperationRepository repo;

    /**
     * Ici, on fait une requête findAll puis on fait des vérifications jugées pertinentes
     * sur le résultat via les différentes assertions. Par exemple sur la taille de la 
     * liste renvoyer, le fait qu'aucune de ses valeurs ne doit être nulle et enfin
     * sur les différentes opération on indique qu'aucun des champs ne peut être null.
     * Ces choix de vérifications sont subjectifs et peuvent tout à fait varier selon
     * les cas, notre modèle de données etc.
     */
    @Test
    void testFindAll() {
        List<Operation> result = repo.findAll();
        assertThat(result)
                .hasSize(4)
                .doesNotContainNull()
                .allSatisfy(operation -> 
                    assertThat(operation).hasNoNullFieldsOrProperties()
                );

                /*
                // à la place du allSatisfy, on peut mettre ça qui va tester la valeur
                //de chaque label de la liste
                 .extracting("label")
                .containsExactlyInAnyOrder("restaurant", "cloth", "gorceries", "gift")
                */
                

    }

    /**
     * Ici pour le findById, on récupère une opération par son id puis on fait des
     * assertions différentes, c'est surtout pour montrer plusieurs manières de faire.
     * On test la valeur exacte de chaque champ de l'opération, mais on aurait pu aussi
     * juste faire comme dans la méthode d'avant et garder uniquement le hasNoNullFieldsOrProperties
     */
    @Test
    void testFindById() {
        Operation operation = repo.findById(1);
        assertThat(operation)
        .hasNoNullFieldsOrProperties()
        .hasFieldOrPropertyWithValue("label", "restaurant")
        .hasFieldOrPropertyWithValue("amount", -25.00)
        .hasFieldOrPropertyWithValue("date", LocalDate.of(2022, 5, 1));
    }

    /**
     * On fait ici un test pour indiquer le comportement attendu lors d'un appel à un
     * id inexistant
     */
    @Test
    void testFindByIdNotExists() {
        Operation operation = repo.findById(10000);
        assertThat(operation).isNull();
    }

    @Test
    void testSave() {
        Operation operation = 
            new Operation("from test", LocalDate.of(2022, 10, 1), 10.00);

        repo.save(operation);

        assertThat(operation.getId()).isNotNull();
    }
}
